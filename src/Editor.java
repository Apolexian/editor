import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.JPopupMenu;

import com.inet.jortho.SpellCheckerOptions;
import com.inet.jortho.PopupListener;
import com.inet.jortho.SpellChecker;
import com.inet.jortho.FileUserDictionary;

public class Editor extends JFrame implements ActionListener {
    JTextArea text;
    JFrame frame;

    void editor() {
        // Spell checker loading dictionary files
        try {
            String userDictionaryPath = "/dictionary/";
            SpellChecker.setUserDictionaryProvider(new FileUserDictionary(userDictionaryPath));
            SpellChecker.registerDictionaries(getClass().getResource(userDictionaryPath), "en");
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Spell checker configs
        SpellCheckerOptions sco = new SpellCheckerOptions();
        sco.setCaseSensitive(false);
        sco.setSuggestionsLimitMenu(10);
        sco.setLanguageDisableVisible(false);
        sco.setIgnoreAllCapsWords(true);
        sco.setIgnoreWordsWithNumbers(true);

        JPopupMenu popup = SpellChecker.createCheckerPopup(sco);
        // To add logo
        ImageIcon img = new ImageIcon("assets/frame_icon.png");

        frame = new JFrame("Editor");
        frame.setIconImage(img.getImage());
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");

        } catch (Exception e) {
            e.printStackTrace();
        }
        text = new JTextArea();
        text.addMouseListener(new PopupListener(popup));
        SpellChecker.register(text);
        JMenuBar menuBar = new JMenuBar();

        // creating the File sub menu that deals with saving/opening/creating new
        JMenu mFile = new JMenu("File");
        JMenuItem miNew = new JMenuItem("New");
        JMenuItem miOpen = new JMenuItem("Open");
        JMenuItem miSave = new JMenuItem("Save");
        miNew.addActionListener(this);
        miOpen.addActionListener(this);
        miSave.addActionListener(this);
        mFile.add(miNew);
        mFile.add(miOpen);
        mFile.add(miSave);

        // creating the Edit submenu that deals with cut/copy/paste
        JMenu mEdit = new JMenu("Edit");
        JMenuItem miCut = new JMenuItem("Cut");
        JMenuItem miCopy = new JMenuItem("Copy");
        JMenuItem miPaste = new JMenuItem("Paste");
        miCut.addActionListener(this);
        miCopy.addActionListener(this);
        miPaste.addActionListener(this);
        mEdit.add(miCut);
        mEdit.add(miCopy);
        mEdit.add(miPaste);

        JMenuItem mClose = new JMenuItem("Close");
        mClose.addActionListener(this);

        // adding everything to menu bar
        menuBar.add(mFile);
        menuBar.add(mEdit);
        menuBar.add(mClose);

        // frame creation
        frame.setJMenuBar(menuBar);
        frame.add(text);
        frame.setSize(600, 600);
        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String action = actionEvent.getActionCommand();
        switch (action) {
            case "Cut":
                text.cut();
                break;
            case "Copy":
                text.copy();
                break;
            case "Paste":
                text.paste();
                break;
            case "Save": {
                JFileChooser fileChooser = new JFileChooser("File:");
                int r = fileChooser.showSaveDialog(null);
                if (r == JFileChooser.APPROVE_OPTION) {
                    File file = new File(String.valueOf(fileChooser.getSelectedFile().getAbsoluteFile()));
                    try {
                        FileWriter writer = new FileWriter(file, false);
                        BufferedWriter bufferedWriter = new BufferedWriter(writer);
                        bufferedWriter.write(text.getText());
                        bufferedWriter.flush();
                        bufferedWriter.close();
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(frame, e.getMessage());
                    }
                } else {
                    JOptionPane.showMessageDialog(frame, "Canceled");
                }
                break;
            }
            case "Open": {
                JFileChooser fileChooser = new JFileChooser("File:");
                int r = fileChooser.showOpenDialog(null);
                if (r == JFileChooser.APPROVE_OPTION) {
                    File file = new File(String.valueOf(fileChooser.getSelectedFile().getAbsoluteFile()));
                    try {
                        String tempString;
                        StringBuilder sl;
                        FileReader fileReader = new FileReader(file);
                        BufferedReader bufferedReader = new BufferedReader(fileReader);
                        sl = new StringBuilder(bufferedReader.readLine());
                        while ((tempString = bufferedReader.readLine()) != null) {
                            sl.append("\n").append(tempString);
                        }
                        text.setText(sl.toString());

                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(frame, e.getMessage());
                    }
                } else {
                    JOptionPane.showMessageDialog(frame, "Cancelled");
                }
                break;
            }
            case "New":
                text.setText("");
                break;
            case "Close":
                frame.setVisible(false);
                System.exit(0);
                break;
        }
    }

    public static void main(String[] args) {
        Editor editor = new Editor();
        editor.editor();
    }
}
